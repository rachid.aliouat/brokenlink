<?php

include 'config.php';
include 'remplirtab.php';


$page="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
$page.="<html>\n";
$page.= "<head>\n";
$page.="<link rel=\"stylesheet\" href=\"charte.css\" />\n";
$page.= "<title>Lien brisé ou autre problème</title>\n";
$page.="    <script src=\"https://www.google.com/recaptcha/api.js\" async defer></script>\n";
$page.="<meta name=\"robots\" content=\"noindex,nofollow\" />\n";
$page.="<meta name=\"googlebot\" content=\"noarchive\" />\n";

$url_redirect="Recupération de tous les parametres OpenURL <br>" . $_SERVER['REQUEST_URI'];
$tab_champs = explode("&", $_SERVER['REQUEST_URI']);


$tableau_html="";

for($i = 1; $i < count($tab_champs); ++$i) {
	$tuple=explode("=",$tab_champs[$i]);

	if (strlen($tuple[1])>0) {
	$libelle=$tab_ref[$tuple[0]];

       $tableau_html.="<label>".$libelle." :    </label>";

	$tableau_html.="<input name=\"".$tuple[0]."\""." id=\"".$tuple[0]."\"". " type=\"text\" readonly=\"readonly\" value=\"". urldecode($tuple[1])."\" size=\"".strlen($tuple[1])."\" style=\"background-color:#e3e3e3;\">\n";
	$tableau_html.="<br><br>\n";
	}

}


$page.= "</head>\n";
$page.= "<body>\n";
$page.="<div id=\"header\">\n";
$page.="		<div id=\"headHaut\">\n";
$page.="				<div id=\"application\" class=\"logoUL100px\">\n";
$page.="				</div>\n";
$page.="				<ul id=\"iconeMenu\">\n";
$page.="					<li>\n";
$page.="						<form id=\"deconn\" action=\"".$url_catalogue."\" method=\"post\">\n";
$page.="							<input type=\"submit\" value=\"Déconnexion\" name=\"deconnexion\" class=\"deco22px\" />\n";
$page.="						</form>\n";
$page.="					</li>\n";
$page.="			</ul>\n";
$page.="		</div>\n";
$page.="          <div id=\"bandeau\" >\n";
$page.="               <h1>Signaler un lien brisé :</h1>\n";
$page.="                     <h5>Si vous voulez signaler un problème, <b>vous pouvez renseigner plus bas votre courriel</b> et ajouter un commentaire.</h5>\n";
//$page.="                     <h5>Si vous voulez signaler un problème, <b>vous pouvez renseigner plus bas votre courriel</b> et ajouter un commentaire. <br><b><i>Une réponse sera apportée à compter du 3 janvier</i></b></h5>\n";
$page.="           </div>\n";
$page.="       </div>\n";

$page.="	<div id=\"conteneurMenu\">\n";
$page.="					</div>\n";
$page.="         <div id=\"conteneur\">\n";
$page.="                     <div id=\"blocMessage\">\n";
$page.="<!-- Message général contexte informatif (fond vert) -->\n";
$page.="					<div id=\"generalInformatif\">\n";
$page.="					</div>\n";
$page.="<!-- Message général contexte d'erreur (fond rouge) -->\n";
$page.="		<div id=\"generalErreur\">\n";
$page.="		</div>\n";
$page.="               <div class=\"clear\">\n";
$page.="               </div>\n";
$page.="  	 </div>\n";
$page.="       <div id=\"contenu\">\n";
$page.="<!-- Contenu  -->\n";


$page.="<div class=\"blocBase\">\n";
$page.="	<div class=\"blocBaseContenu\">\n";
$page.= "<center>\n";
//$page.= "<H1>Signaler un lien brisé ou un autre problème </H1>\n";
//$page.= "<p>".$url_redirect . "</p>\n"; 
$page.= "</center>\n";
//$page.= "<p>Analyse pour en faire une liste : </p><br><br>\n";
//$page.=$liste;
//$page.="<p>Passage en mode tableau : </p><br><br>\n";
$page.="<form name=\"formulaire\" action=\"envoi-donnees.php\" method=\"POST\" enctype=\"multipart/form-data\">\n";
$page.=$tableau_html;
$page.="<p>\n";
$page.="<label class=\"fushia\"><b>Courriel*</b> : </label><input name=\"mail\" id=\"mail\" type=\"text\" size=\"50\">\n";
$page.="</p>\n";
$page.="<i><b class=\"fushia\">*précisez votre courriel pour avoir une réponse</i></b>\n";
$page.="</p>\n";
$page.="<p>\n";
$page.="<legend>Commentaires :</legend>\n";
$page.="<textarea  name=\"commentaire\" id=\"commentaire\" class=\"commentaire\" style=\"width:600px;font-size:15px; margin:0px; padding-left:2px;padding-right: 3px\"></textarea>\n";
$page.="</p>\n";
$page.="      <div class=\"g-recaptcha\" data-sitekey=\"". $cle_captcha_public  ."\"></div>"; //la clé captcha est chargée par config.php (voir include plus haut)
$page.="<center><input type=\"submit\" class=\"bUpec boutonTexte btnEnregistrer\" value=\"Envoyer\"></center>\n";
$page.="</form>\n";
$page.="</div>\n";
$page.="</div>\n";
$page.="</div>\n";
$page.= "</body>\n";
$page.="</html>\n";
echo $page;

?>
