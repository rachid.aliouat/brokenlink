<?php

include 'config.php';
include 'remplirtab.php';

setlocale(LC_TIME, 'fr_FR');
date_default_timezone_set('Europe/Paris');
$date_jour= utf8_encode(strftime('%A %d %B %Y, %H:%M'));

// preparation page à afficher in fine
$page="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
$page.="<html>\n";
$page.= "<head>\n";
$page.="<link rel=\"stylesheet\" href=\"charte.css\" />\n";
$page.= "<title>Lien brisé ou autre problème</title>\n";
$page.="<meta name=\"robots\" content=\"noindex,nofollow\" />\n";
$page.="<meta name=\"googlebot\" content=\"noarchive\" />\n";
$page.= "</head>\n";
$page.= "<body>\n";
$page.="<div id=\"header\">\n";
$page.="		<div id=\"headHaut\">\n";
$page.="				<div id=\"application\" class=\"logoUL100px\">\n";
$page.="				</div>\n";
$page.="				<ul id=\"iconeMenu\">\n";
$page.="					<li>\n";
$page.="						<form id=\"deconn\" action=\"".$url_catalogue."\" method=\"post\">\n";
$page.="							<input type=\"submit\" value=\"Déconnexion\" name=\"deconnexion\" class=\"deco22px\" />\n";
$page.="						</form>\n";
$page.="					</li>\n";
$page.="			</ul>\n";
$page.="		</div>\n";
$page.="          <div id=\"bandeau\" >\n";
$page.="               <h1>Signaler un lien brisé ou un autre problème :</h1>\n";
//$page.="                     <h5>Si vous voulez signaler un problème, vous pouvez renseigner plus bas votre email(optionnel) et ajouter un commentaire.</h5>\n";
$page.="           </div>\n";
$page.="       </div>\n";
$page.="	<div id=\"conteneurMenu\">\n";
$page.="					</div>\n";
$page.="         <div id=\"conteneur\">\n";
$page.="                     <div id=\"blocMessage\">\n";
$page.="<!-- Message général contexte informatif (fond vert) -->\n";
$page.="					<div id=\"generalInformatif\">\n";
$page.="					</div>\n";
$page.="<!-- Message général contexte d'erreur (fond rouge) -->\n";
$page.="		<div id=\"generalErreur\">\n";
$page.="		</div>\n";
$page.="               <div class=\"clear\">\n";
$page.="               </div>\n";
$page.="  	 </div>\n";
$page.="       <div id=\"contenu\">\n";
$page.="<!-- Contenu  -->\n";
$page.="<div class=\"blocBase\">\n";
$page.="	<div class=\"blocBaseContenu\">\n";

$page_fin="<form>\n";
$page_fin.="<p class=\"bBleu boutonTexte btnEnregistrer\">\n";
//$page_fin.="<button class=\"bBleu boutonTexte btnEnregistrer\" onclick=\"history.back()\" >Retour</button>\n";
$page_fin.="<input type=\"button\" value=\"Retour\"  class=\"modif js-history\" style=\"box-shadow:inset 0px 1px 0px 0px #ffffff;background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);background-color:#ffffff;border-radius:6px;border:1px solid #dcdcdc;display:inline-block;cursor:pointer;color:#666666;font-family:Arial;font-size:15px;font-weight:bold;padding:6px 24px;text-decoration:none;text-shadow:0px 1px 0px #ffffff;\">\n";
$page_fin.="</p>\n";
$page_fin.="</form>\n";
$page_fin.="</div>\n";
$page_fin.="</div>\n";

$page_fin.="        <script> \n";
$page_fin.="var btnBack = document.querySelector('.js-history');\n";
$page_fin.="btnBack.addEventListener('click', function () {\n";
$page_fin.="    window.history.back();\n";
$page_fin.="});\n";
$page_fin.="        </script>\n";

$page_fin.= "</body>\n";
$page_fin.="</html>\n";



//fin preparation page a afficher in fine







$mail = $_POST['mail'] ;
// test si mail pas valide, alors mail = from_mail
if (!(filter_var($mail, FILTER_VALIDATE_EMAIL))) { //verifie si email conforme
        $mail=$from_mail;
      }
$commentaire = $_POST['commentaire'] ;
$info_bib="";


$body="<html><head></head><body>\n";
//$body.="<i>".date('l jS \of F Y h:i:s A')."</i><br>";
$body.="<i>". $date_jour ."</i><br>";

$body.="<h3>Informations bibliographiques liées au problème :</h3>\n";

$doi_exist=0;
$mmsid_exist=0;
foreach ($_POST as $key => $value){
	$code = str_replace("_", ".", $key);
	$libelle=$tab_ref[$code]; //tab_ref est alimentée par remplirtab.php (voir include plus haut)
	if (strcmp($code, "mail") == 0) {
		$info_bib.="<hr><h3>Informations sur le lecteur : </h3>\n";
	}
	if (strcmp($code, "rft.doi") == 0) {
                $doi_exist=1;
		$doi=urldecode($value);
        }

	if (strcmp($code, "rft.mms.id") == 0) {
		$mmsid_exist=1;
                $num_notice=urldecode($value);

        }


	if (strlen($libelle)>0) { //code inconnu ou code du ticket captcha : on ne prend pas
      		$info_bib.="<b>".$libelle.":</b>". urldecode($value) ."<br>\n";
	}

//  echo "{$key}--{$code}--{$long_code}-- => {$value} <br>\n";
//    echo "{$libelle} => {$value} <br>\n";

//    print_r($arr);
}


$body.=$info_bib;
$body.="<hr>\n";
//$body.="<a href=\"".$url_catalogue."\" rel=\"noopener noreferrer\" target=\"_blank\">Lien vers le catalogue</a> \n";
if ($mmsid_exist==1) {
        $body.="<br><a href=\"".$url_perenne_mmsid.$num_notice."\" rel=\"noopener noreferrer\" target=\"_blank\">Lien direct via le MMS ID</a> \n";
	}
elseif ($doi_exist==1) {
	$body.="<br><a href=\"".$url_catalogue_search.$doi."\" rel=\"noopener noreferrer\" target=\"_blank\">Lien direct via le DOI</a> \n";

	} 
else
	{
	$body.="<br><a href=\"".$url_catalogue."\" rel=\"noopener noreferrer\" target=\"_blank\">Lien vers le catalogue</a> \n";


}
$body.="</body></html>\n";

//echo $body;

        // Ma clé privée
        $secret = $cle_captcha_secret; //chargée par le fichier config.php (voir include plus haut)
        // Paramètre renvoyé par le recaptcha
        $response = $_POST['g-recaptcha-response'];
        // On récupère l'IP de l'utilisateur
        $remoteip = $_SERVER['REMOTE_ADDR'];

        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
            . $secret
            . "&response=" . $response
            . "&remoteip=" . $remoteip ;

        $decode = json_decode(file_get_contents($api_url), true);

        if ($decode['success'] == true) {
                // C'est un humain
                // echo "Capcha OK\n";
        }

        else {
              	// C'est un robot ou le code de vérification est incorrecte
                //exit("Vous n'avez pas validé le captcha 'Je ne suis pas un robot'.");
		echo $page;
                echo "Vous n'avez pas validé le captcha \"Je ne suis pas un robot\".\n";
                echo "<img class=\"logo\" src=\"images/mail-not-ok.png\" alt=\"Mail NOT OK\" title=\"Mail NOT OK\" ><br>\n";

		echo $page_fin;
		exit(); // on arrete tout
		
        }

     //if (filter_var($mail, FILTER_VALIDATE_EMAIL)) { //verifie si email conforme
	//	$from_mail=$mail;
	//	}


     $headers ='From: '.$from_mail.'<'.$from_mail .'>'."\n";
     $headers .='Reply-To: '.$mail."\n";
     $headers .='Content-Type: text/html; charset="utf-8"'."\n";
     $headers .='Content-Transfer-Encoding: 8bit';
     
    //    echo "<html><head></head><body><br>\n";
    //  reencodage en mime car utf8 non supporté par certain serveur postfix
    //$obj_mail=iconv_mime_encode('Subject',$obj_mail);
    
     if(mail($dest_mail , $obj_mail , $body, $headers))
     {
          //echo "<h3> >>> Le message a bien été envoyé</h3><br>"."\n";
	echo $page;
	  echo "<img class=\"logo\" src=\"images/mail-ok.png\" alt=\"Mail OK\" title=\"Mail OK\" ><br>\n";
          echo "<b>Le message a bien été envoyé</b><br>"."\n";
	echo $page_fin;
// envoi le copie du mail a dsi doc technique
     mail($dest_mail_copie , $obj_mail , $body, $headers);


     }
     else
     {
        //echo "<html><head></head><body><br>\n";
	echo $page;
        echo '<h3>Le message n\'a pu être envoyé</h3>';
        echo "<img class=\"logo\" src=\"images/mail-not-ok.png\" alt=\"Mail NOT OK\" title=\"Mail NOT OK\" ><br>\n";
	echo $page_fin;


     }


?>

